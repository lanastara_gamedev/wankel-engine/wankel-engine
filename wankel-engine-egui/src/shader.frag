
in vec2 v_uv;
in vec4 v_color;
in vec2 v_clip_min;
in vec2 v_clip_max;
out vec4 frag;

uniform sampler2D tex;

void main() {

    vec4 color = texture(tex, v_uv) * v_color;
    if(color.w == 0.0)
    {
        discard;
    }

    if(gl_FragCoord.x < v_clip_min.x)
    {
        discard;
    }

    if(gl_FragCoord.y < v_clip_min.y)
    {
        discard;
    }

    if(gl_FragCoord.x > v_clip_max.x)
    {
        discard;
    }

    if(gl_FragCoord.y > v_clip_max.y)
    {
        discard;
    }
    
    frag = color;
}