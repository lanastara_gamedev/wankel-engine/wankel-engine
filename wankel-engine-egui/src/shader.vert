in float depth;
in vec2 pos;
in vec2 uv;
in vec4 color;
in vec2 clip_min;
in vec2 clip_max;

uniform mat4 view_projection;

out vec2 v_uv;
out vec4 v_color;
out vec2 v_clip_min;
out vec2 v_clip_max;

void main() {
    gl_Position = view_projection * vec4(pos, depth, 1.0);
    v_uv = uv;
    v_color = color;
    v_clip_min = clip_min;
    v_clip_max = clip_max;
}