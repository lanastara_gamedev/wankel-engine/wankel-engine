pub use egui;
use egui::{Modifiers, TouchDeviceId};

use wankel_engine::winit::event::{ElementState, Touch, WindowEvent};

use copypasta::{ClipboardContext, ClipboardProvider};

use crate::EguiLayerCacheInner;

fn get_egui_modifiers(modifiers: &wankel_engine::winit::event::ModifiersState) -> egui::Modifiers {
    egui::Modifiers {
        alt: modifiers.alt(),
        command: modifiers.logo(),
        ctrl: modifiers.ctrl(),
        shift: modifiers.shift(),
        mac_cmd: false,
    }
}

pub(crate) fn get_egui_event(
    e: &WindowEvent,
    cache: &mut EguiLayerCacheInner,
) -> Option<egui::Event> {
    let EguiLayerCacheInner {
        ref mut cursor_position,
        ref mut modifiers,
        ref mut pixels_per_point,
        ..
    } = cache;

    match e {
        wankel_engine::winit::event::WindowEvent::ReceivedCharacter(c) => {
            if c.is_control() {
                None
            } else {
                Some(egui::Event::Text(c.to_string()))
            }
        }
        wankel_engine::winit::event::WindowEvent::KeyboardInput { input, .. } => {
            match (*modifiers, input.virtual_keycode) {
                (Modifiers { ctrl: true, .. }, Some(wankel_engine::winit::event::VirtualKeyCode::C)) => {
                    return Some(egui::Event::Copy)
                }
                (Modifiers { ctrl: true, .. }, Some(wankel_engine::winit::event::VirtualKeyCode::X)) => {
                    return Some(egui::Event::Cut)
                }
                (Modifiers { ctrl: true, .. }, Some(wankel_engine::winit::event::VirtualKeyCode::V)) => {
                    let mut clipboard = ClipboardContext::new().unwrap();
                    let content = clipboard.get_contents().unwrap().replace("\r\n", "\n");

                    if !content.is_empty() {
                        return Some(egui::Event::Paste(content));
                    }
                }

                _ => {}
            }

            Some(egui::Event::Key {
                key: get_egui_key(input.virtual_keycode, input.scancode)?,
                pressed: input.state == ElementState::Pressed,
                modifiers: modifiers.clone(),
            })
        }
        wankel_engine::winit::event::WindowEvent::ModifiersChanged(modi) => {
            *modifiers = get_egui_modifiers(modi);
            None
        }
        wankel_engine::winit::event::WindowEvent::CursorMoved { position, .. } => {
            *cursor_position = egui::Pos2::new(position.x as f32, position.y as f32);
            Some(egui::Event::PointerMoved(cursor_position.clone()))
        }

        wankel_engine::winit::event::WindowEvent::CursorLeft { .. } => {
            Some(egui::Event::PointerGone)
        }
        wankel_engine::winit::event::WindowEvent::MouseWheel { delta, .. } => {
            let delta = match delta {
                wankel_engine::winit::event::MouseScrollDelta::LineDelta(px, py) => {
                    egui::Vec2::new(*px, -*py) * 50.0
                }
                wankel_engine::winit::event::MouseScrollDelta::PixelDelta(p) => {
                    egui::Vec2::new(p.x as f32, p.y as f32) / *pixels_per_point
                }
            };

            if modifiers.ctrl || modifiers.command {
                let factor = (delta.y / 200.0).exp();
                Some(egui::Event::Zoom(factor))
            } else if modifiers.shift {
                Some(egui::Event::Scroll(egui::vec2(delta.x + delta.y, 0.0)))
            } else {
                Some(egui::Event::Scroll(delta))
            }
        }
        wankel_engine::winit::event::WindowEvent::MouseInput { state, button, .. } => {
            Some(egui::Event::PointerButton {
                pos: cursor_position.clone(),
                button: match button {
                    wankel_engine::winit::event::MouseButton::Left => egui::PointerButton::Primary,
                    wankel_engine::winit::event::MouseButton::Right => {
                        egui::PointerButton::Secondary
                    }
                    wankel_engine::winit::event::MouseButton::Middle => egui::PointerButton::Middle,
                    wankel_engine::winit::event::MouseButton::Other(1) => {
                        egui::PointerButton::Extra1
                    }
                    wankel_engine::winit::event::MouseButton::Other(2) => {
                        egui::PointerButton::Extra2
                    }
                    _ => {
                        return None;
                    }
                },
                pressed: *state == ElementState::Pressed,
                modifiers: modifiers.clone(),
            })
        }
        wankel_engine::winit::event::WindowEvent::Touch(Touch {
            device_id,
            force,
            id,
            location,
            phase,
        }) => Some(egui::Event::Touch {
            id: egui::TouchId::from(*id),
            device_id: TouchDeviceId(egui::epaint::util::hash(device_id)),
            phase: match phase {
                wankel_engine::winit::event::TouchPhase::Started => egui::TouchPhase::Start,
                wankel_engine::winit::event::TouchPhase::Moved => egui::TouchPhase::Move,
                wankel_engine::winit::event::TouchPhase::Ended => egui::TouchPhase::End,
                wankel_engine::winit::event::TouchPhase::Cancelled => egui::TouchPhase::Cancel,
            },
            pos: egui::pos2(
                location.x as f32 / *pixels_per_point,
                location.y as f32 / *pixels_per_point,
            ),
            force: match force {
                Some(wankel_engine::winit::event::Force::Normalized(force)) => *force as f32,
                Some(wankel_engine::winit::event::Force::Calibrated {
                    force,
                    max_possible_force,
                    ..
                }) => (force / max_possible_force) as f32,
                None => 0_f32,
            },
        }),
        wankel_engine::winit::event::WindowEvent::ScaleFactorChanged {
            scale_factor, ..
        } => {
            *pixels_per_point = *scale_factor as f32;
            None
        }

        _ => None,
    }
}

fn get_egui_key(
    virtual_keycode: Option<wankel_engine::winit::event::VirtualKeyCode>,
    _scan_code: u32,
) -> Option<egui::Key> {
    match virtual_keycode {
        Some(key) => match key {
            wankel_engine::winit::event::VirtualKeyCode::Key1 => Some(egui::Key::Num1),
            wankel_engine::winit::event::VirtualKeyCode::Key2 => Some(egui::Key::Num2),
            wankel_engine::winit::event::VirtualKeyCode::Key3 => Some(egui::Key::Num3),
            wankel_engine::winit::event::VirtualKeyCode::Key4 => Some(egui::Key::Num4),
            wankel_engine::winit::event::VirtualKeyCode::Key5 => Some(egui::Key::Num5),
            wankel_engine::winit::event::VirtualKeyCode::Key6 => Some(egui::Key::Num6),
            wankel_engine::winit::event::VirtualKeyCode::Key7 => Some(egui::Key::Num7),
            wankel_engine::winit::event::VirtualKeyCode::Key8 => Some(egui::Key::Num8),
            wankel_engine::winit::event::VirtualKeyCode::Key9 => Some(egui::Key::Num9),
            wankel_engine::winit::event::VirtualKeyCode::Key0 => Some(egui::Key::Num0),
            wankel_engine::winit::event::VirtualKeyCode::A => Some(egui::Key::A),
            wankel_engine::winit::event::VirtualKeyCode::B => Some(egui::Key::B),
            wankel_engine::winit::event::VirtualKeyCode::C => Some(egui::Key::C),
            wankel_engine::winit::event::VirtualKeyCode::D => Some(egui::Key::D),
            wankel_engine::winit::event::VirtualKeyCode::E => Some(egui::Key::E),
            wankel_engine::winit::event::VirtualKeyCode::F => Some(egui::Key::F),
            wankel_engine::winit::event::VirtualKeyCode::G => Some(egui::Key::G),
            wankel_engine::winit::event::VirtualKeyCode::H => Some(egui::Key::H),
            wankel_engine::winit::event::VirtualKeyCode::I => Some(egui::Key::I),
            wankel_engine::winit::event::VirtualKeyCode::J => Some(egui::Key::J),
            wankel_engine::winit::event::VirtualKeyCode::K => Some(egui::Key::K),
            wankel_engine::winit::event::VirtualKeyCode::L => Some(egui::Key::L),
            wankel_engine::winit::event::VirtualKeyCode::M => Some(egui::Key::M),
            wankel_engine::winit::event::VirtualKeyCode::N => Some(egui::Key::N),
            wankel_engine::winit::event::VirtualKeyCode::O => Some(egui::Key::O),
            wankel_engine::winit::event::VirtualKeyCode::P => Some(egui::Key::P),
            wankel_engine::winit::event::VirtualKeyCode::Q => Some(egui::Key::Q),
            wankel_engine::winit::event::VirtualKeyCode::R => Some(egui::Key::R),
            wankel_engine::winit::event::VirtualKeyCode::S => Some(egui::Key::S),
            wankel_engine::winit::event::VirtualKeyCode::T => Some(egui::Key::T),
            wankel_engine::winit::event::VirtualKeyCode::U => Some(egui::Key::U),
            wankel_engine::winit::event::VirtualKeyCode::V => Some(egui::Key::V),
            wankel_engine::winit::event::VirtualKeyCode::W => Some(egui::Key::W),
            wankel_engine::winit::event::VirtualKeyCode::X => Some(egui::Key::X),
            wankel_engine::winit::event::VirtualKeyCode::Y => Some(egui::Key::Y),
            wankel_engine::winit::event::VirtualKeyCode::Z => Some(egui::Key::Z),
            wankel_engine::winit::event::VirtualKeyCode::Escape => Some(egui::Key::Escape),
            wankel_engine::winit::event::VirtualKeyCode::F1 => Some(egui::Key::F1),
            wankel_engine::winit::event::VirtualKeyCode::F2 => Some(egui::Key::F2),
            wankel_engine::winit::event::VirtualKeyCode::F3 => Some(egui::Key::F3),
            wankel_engine::winit::event::VirtualKeyCode::F4 => Some(egui::Key::F4),
            wankel_engine::winit::event::VirtualKeyCode::F5 => Some(egui::Key::F5),
            wankel_engine::winit::event::VirtualKeyCode::F6 => Some(egui::Key::F6),
            wankel_engine::winit::event::VirtualKeyCode::F7 => Some(egui::Key::F7),
            wankel_engine::winit::event::VirtualKeyCode::F8 => Some(egui::Key::F8),
            wankel_engine::winit::event::VirtualKeyCode::F9 => Some(egui::Key::F9),
            wankel_engine::winit::event::VirtualKeyCode::F10 => Some(egui::Key::F10),
            wankel_engine::winit::event::VirtualKeyCode::F11 => Some(egui::Key::F11),
            wankel_engine::winit::event::VirtualKeyCode::F12 => Some(egui::Key::F12),
            wankel_engine::winit::event::VirtualKeyCode::F13 => Some(egui::Key::F13),
            wankel_engine::winit::event::VirtualKeyCode::F14 => Some(egui::Key::F14),
            wankel_engine::winit::event::VirtualKeyCode::F15 => Some(egui::Key::F15),
            wankel_engine::winit::event::VirtualKeyCode::F16 => Some(egui::Key::F16),
            wankel_engine::winit::event::VirtualKeyCode::F17 => Some(egui::Key::F17),
            wankel_engine::winit::event::VirtualKeyCode::F18 => Some(egui::Key::F18),
            wankel_engine::winit::event::VirtualKeyCode::F19 => Some(egui::Key::F19),
            wankel_engine::winit::event::VirtualKeyCode::F20 => Some(egui::Key::F20),
            wankel_engine::winit::event::VirtualKeyCode::Insert => Some(egui::Key::Insert),
            wankel_engine::winit::event::VirtualKeyCode::Home => Some(egui::Key::Home),
            wankel_engine::winit::event::VirtualKeyCode::Delete => Some(egui::Key::Delete),
            wankel_engine::winit::event::VirtualKeyCode::End => Some(egui::Key::End),
            wankel_engine::winit::event::VirtualKeyCode::PageDown => Some(egui::Key::PageDown),
            wankel_engine::winit::event::VirtualKeyCode::PageUp => Some(egui::Key::PageUp),
            wankel_engine::winit::event::VirtualKeyCode::Left => Some(egui::Key::ArrowLeft),
            wankel_engine::winit::event::VirtualKeyCode::Up => Some(egui::Key::ArrowUp),
            wankel_engine::winit::event::VirtualKeyCode::Right => Some(egui::Key::ArrowRight),
            wankel_engine::winit::event::VirtualKeyCode::Down => Some(egui::Key::ArrowDown),
            wankel_engine::winit::event::VirtualKeyCode::Back => Some(egui::Key::Backspace),
            wankel_engine::winit::event::VirtualKeyCode::Return => Some(egui::Key::Enter),
            wankel_engine::winit::event::VirtualKeyCode::Space => Some(egui::Key::Space),
            wankel_engine::winit::event::VirtualKeyCode::Numpad0 => Some(egui::Key::Num0),
            wankel_engine::winit::event::VirtualKeyCode::Numpad1 => Some(egui::Key::Num1),
            wankel_engine::winit::event::VirtualKeyCode::Numpad2 => Some(egui::Key::Num2),
            wankel_engine::winit::event::VirtualKeyCode::Numpad3 => Some(egui::Key::Num3),
            wankel_engine::winit::event::VirtualKeyCode::Numpad4 => Some(egui::Key::Num4),
            wankel_engine::winit::event::VirtualKeyCode::Numpad5 => Some(egui::Key::Num5),
            wankel_engine::winit::event::VirtualKeyCode::Numpad6 => Some(egui::Key::Num6),
            wankel_engine::winit::event::VirtualKeyCode::Numpad7 => Some(egui::Key::Num7),
            wankel_engine::winit::event::VirtualKeyCode::Numpad8 => Some(egui::Key::Num8),
            wankel_engine::winit::event::VirtualKeyCode::Numpad9 => Some(egui::Key::Num9),
            wankel_engine::winit::event::VirtualKeyCode::NumpadEnter => Some(egui::Key::Enter),
            wankel_engine::winit::event::VirtualKeyCode::Tab => Some(egui::Key::Tab),
            _ => None,
        },
        None => None,
    }
}
