use std::{
    collections::HashMap,
    marker::PhantomData,
    pin::Pin,
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc,
    },
};

use anymap::{any::Any, Map};
use futures::{task::Poll, Future};
use image::{DynamicImage, ImageFormat};
use parking_lot::{
    MappedRwLockReadGuard, MappedRwLockWriteGuard, RwLock, RwLockReadGuard, RwLockWriteGuard,
};
use wankel_engine_platform::{io::IOContext, tracing::{self, instrument, error, info}};

use crate::Handle;

pub trait Asset: Any + Send + Sync {
    type Loader: AssetLoader<Output = Self>;
}

pub trait BaseAssetLoader: Any + Send + Sync {}

pub trait AssetLoader: BaseAssetLoader {
    type Output: Asset;

    fn load(data: Vec<u8>) -> Option<Self::Output>;
}

impl<T> BaseAssetLoader for T where T: AssetLoader {}

#[derive(Clone)]
pub struct Assets {
    next_handle: Arc<AtomicUsize>,
    assets: Arc<RwLock<Map<dyn Any + Send + Sync>>>,
}

#[derive(Debug, Clone)]
pub enum LoadState<T> {
    Loading,
    Error,
    Loaded(T),
}

pub struct Bundle {}

impl Default for Assets {
    fn default() -> Self {
        Self {
            next_handle: Arc::new(AtomicUsize::new(0)),
            assets: Arc::new(RwLock::new(Map::new())),
        }
    }
}

impl Assets {
    pub fn get<THandle: Send + Sync + 'static>(
        &self,
        handle: &Handle<THandle>,
    ) -> Option<MappedRwLockReadGuard<LoadState<THandle>>> {
        let lock = self.assets.read();

        let assets = RwLockReadGuard::try_map(lock, |assets| {
            let assets = assets.get::<HashMap<Handle<THandle>, LoadState<THandle>>>();
            if let Some(assets) = assets {
                assets.get(handle)
            } else {
                None
            }
        })
        .ok();

        assets
    }

    pub fn get_mut<THandle: Send + Sync + 'static>(
        &mut self,
        handle: &Handle<THandle>,
    ) -> Option<MappedRwLockWriteGuard<LoadState<THandle>>> {
        let lock = self.assets.write();

        let assets = RwLockWriteGuard::try_map(lock, |assets| {
            let assets = assets.get_mut::<HashMap<Handle<THandle>, LoadState<THandle>>>();
            if let Some(assets) = assets {
                assets.get_mut(handle)
            } else {
                None
            }
        })
        .ok();

        assets
    }

    fn get_handle<T>(&self) -> Handle<T> {
        Handle(
            self.next_handle.fetch_add(1, Ordering::SeqCst),
            PhantomData::default(),
        )
    }

    pub fn create_asset<THandle: Send + Sync + 'static>(&self) -> Handle<THandle> {
        let mut lock = self.assets.write();

        let handle = self.get_handle();

        if let Some(assets) = lock.get_mut::<HashMap<Handle<THandle>, LoadState<THandle>>>() {
            assets.insert(handle, LoadState::Loading);
        } else {
            let mut assets: HashMap<Handle<THandle>, LoadState<THandle>> = HashMap::new();
            assets.insert(handle, LoadState::Loading);
            lock.insert(assets);
        }

        handle
    }
}

pub struct ImageLoader;

impl AssetLoader for ImageLoader {
    type Output = DynamicImage;

    fn load(data: Vec<u8>) -> Option<Self::Output> {
        image::load_from_memory_with_format(&data, ImageFormat::Png).ok()
    }
}

impl Asset for DynamicImage {
    type Loader = ImageLoader;
}

pub struct FontLoader;

pub struct Font(pub(crate) glyph_brush::ab_glyph::FontArc);

impl Asset for Font {
    type Loader = FontLoader;
}

impl AssetLoader for FontLoader {
    type Output = Font;

    fn load(data: Vec<u8>) -> Option<Self::Output> {
        Some(Font(
            glyph_brush::ab_glyph::FontArc::try_from_vec(data).ok()?,
        ))
    }
}

pub struct AssetServerBuilder {
    loaders: Map<dyn Any + Send + Sync + 'static>,
}

impl AssetServerBuilder {
    pub fn register_loader<T: BaseAssetLoader + 'static>(&mut self, loader: T) -> &mut Self {
        self.loaders.insert(loader);

        self
    }

    pub fn build(self, assets: Assets, io: crate::platform::io::IO) -> AssetServer {
        AssetServer {
            assets,
            asset_loaders: Arc::new(self.loaders),
            io: Arc::new(io),
            tasks: Vec::new(),
        }
    }
}

pub struct AssetServer {
    assets: Assets,
    asset_loaders: Arc<Map<dyn Any + Send + Sync>>,
    io: Arc<crate::platform::io::IO>,
    tasks: Vec<Box<dyn Future<Output = ()>>>,
}

impl AssetServer {
    pub fn builder() -> AssetServerBuilder {
        AssetServerBuilder {
            loaders: Map::new(),
        }
    }

    #[instrument(skip(self))]
    pub fn load<T: Asset>(&mut self, path: &str) -> Handle<T> {
        let mut assets = self.assets.clone();
        let loaders = self.asset_loaders.clone();
        let io = self.io.clone();
        let path = path.to_string();

        let handle = assets.create_asset::<T>();

        let x = Box::new(async move {
            if let Some(_loader) = loaders.get::<T::Loader>() {
                let data: Option<Vec<u8>> = io.open_file(&path).await;

                if let Some(data) = data {
                    let asset = <T as Asset>::Loader::load(data);

                    if let Some(asset) = asset {
                        let asset_ref = assets.get_mut(&handle);

                        if let Some(mut asset_ref) = asset_ref {
                            *asset_ref = LoadState::Loaded(asset);
                            info!("Asset Loaded");
                        }
                    } else {
                        error!(?path, "Error while loading Asset");
                    }
                }
            } else {
                error!("Assetload not registered");
            }
        });

        self.tasks.push(x);

        handle
    }

    pub fn update(&mut self) {
        let mut finished_tasks = Vec::new();

        for (index, task) in self.tasks.iter_mut().enumerate() {
            let waker = futures::task::noop_waker();
            let mut ctx = futures::task::Context::from_waker(&waker);
            let task = task.as_mut();
            let task = unsafe { Pin::new_unchecked(task) };
            match Future::poll(task, &mut ctx) {
                Poll::Pending => {}
                Poll::Ready(_) => {
                    finished_tasks.push(index);
                }
            }
        }

        finished_tasks.reverse();

        for index in finished_tasks {
            let _ = self.tasks.remove(index);
        }
    }
}
