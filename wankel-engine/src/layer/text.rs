use std::collections::HashMap;

use glyph_brush::{
    BuiltInLineBreaker, GlyphCalculatorBuilder, GlyphCruncher, HorizontalAlign, Layout,
};
use wankel_engine_platform::geom::{Point, Rect, Size};
use wgpu::{Color, DepthStencilState, RenderPassDepthStencilAttachment};

use crate::{
    asset::{Assets, Font, LoadState},
    get_color_rgba,
    texture::Texture,
    Handle, PrepareContext, RenderContext,
};

use super::{InitContext, Layer, LayerConstructor, UpdateContext};

pub mod prelude {
    pub use super::{Text, TextLayer, TextSection};
}

pub struct Text {
    text: String,
    scale: f32,
    font: Handle<Font>,
    color: Color,
}

impl Text {
    pub fn new<TText: Into<String>>(text: TText, font: Handle<Font>) -> Self {
        Self {
            text: text.into(),
            scale: 16.0,
            color: Color::BLACK,
            font,
        }
    }

    pub fn set_color(&mut self, color: Color) {
        self.color = color;
    }

    pub fn with_color(mut self, color: Color) -> Self {
        self.set_color(color);
        self
    }

    pub fn set_scale(&mut self, scale: f32) {
        self.scale = scale;
    }

    pub fn with_scale(mut self, scale: f32) -> Self {
        self.set_scale(scale);
        self
    }

    pub fn set_text(&mut self, text: &str) {
        self.text = text.to_string();
    }
}

pub struct TextSection {
    position: Point<f32>,
    bounds: Size<f32>,
    layout: Layout<BuiltInLineBreaker>,
    text: Vec<Text>,
    disabled: bool,
}

impl TextSection {
    pub fn new() -> Self {
        Self {
            position: Point::new(0.0, 0.0),
            bounds: Size::new(f32::INFINITY, f32::INFINITY),
            layout: Layout::default(),
            text: Vec::new(),
            disabled: false,
        }
    }

    pub fn add_text(&mut self, text: Text) {
        self.text.push(text)
    }

    pub fn with_text(mut self, text: Text) -> Self {
        self.add_text(text);
        self
    }

    pub fn set_position(&mut self, position: Point<f32>) {
        self.position = position;
    }

    pub fn with_position(mut self, position: Point<f32>) -> Self {
        self.set_position(position);
        self
    }
    pub fn set_bounds(&mut self, bounds: Size<f32>) {
        self.bounds = bounds;
    }

    pub fn with_bounds(mut self, bounds: Size<f32>) -> Self {
        self.set_bounds(bounds);
        self
    }

    pub fn set_h_aligh(&mut self, align: HorizontalAlign) {
        self.layout = self.layout.h_align(align);
    }

    pub fn with_h_aligh(mut self, align: HorizontalAlign) -> Self {
        self.set_h_aligh(align);
        self
    }

    pub fn get_mut(&mut self, index: usize) -> Option<&mut Text> {
        self.text.get_mut(index)
    }

    pub fn disabled(&self) -> bool {
        self.disabled
    }

    pub fn set_disabled(&mut self, disabled: bool) {
        self.disabled = disabled;
    }

    pub fn measure(&self, ctx: &UpdateContext) -> Option<Rect<f32>> {
        let mut builder: Option<GlyphCalculatorBuilder> = None;

        let mut section = glyph_brush::Section::default()
            .with_layout(self.layout)
            .with_bounds(self.bounds)
            .with_screen_position(self.position);

        for t in &self.text {
            let font = ctx.assets.get(&t.font)?;
            match &*font {
                LoadState::Loading => {
                    return None;
                }
                LoadState::Error => {
                    return None;
                }
                LoadState::Loaded(font) => {
                    let text = match builder.as_mut() {
                        Some(builder) => {
                            let font_id = builder.add_font(font.0.clone());
                            wgpu_glyph::Text::new(t.text.as_str()).with_font_id(font_id)
                        }
                        None => {
                            builder = Some(GlyphCalculatorBuilder::using_font(font.0.clone()));
                            wgpu_glyph::Text::new(t.text.as_str())
                                .with_font_id(wgpu_glyph::FontId(0))
                        }
                    }
                    .with_scale(t.scale);

                    section = section.add_text(text);
                }
            }
        }

        let builder = builder?;

        let calculator = builder.build();

        let mut scope = calculator.cache_scope();

        let bounds = scope
            .glyph_bounds_custom_layout(section, &self.layout)
            .map(|rect| {
                Rect::<f32>::new(
                    rect.min.x,
                    rect.min.y,
                    rect.max.x - rect.min.x,
                    rect.max.y - rect.min.y,
                )
            });

        bounds
    }
}

impl Handle<TextSection> {
    pub fn update<TOut, TFn: FnMut(&mut TextSection) -> TOut>(
        &self,
        layer: &mut TextLayer,
        mut update: TFn,
    ) -> Option<TOut> {
        let texture = layer.sections.get_mut(self.0)?;

        Some(update(texture))
    }
}

pub struct TextLayer {
    font_lookup: HashMap<Handle<Font>, wgpu_glyph::FontId>,
    glyph_brush:
        Option<wgpu_glyph::GlyphBrush<wgpu::DepthStencilState, glyph_brush::ab_glyph::FontArc>>,
    staging_belt: wgpu::util::StagingBelt,
    sections: Vec<TextSection>,
}

impl LayerConstructor for TextLayer {
    type Params = ();

    fn new(_ctx: InitContext, _params: Self::Params) -> Self {
        let font_lookup = HashMap::new();
        let staging_belt = wgpu::util::StagingBelt::new(1024);

        Self {
            font_lookup,
            glyph_brush: None,
            staging_belt,
            sections: Default::default(),
        }
    }
}

impl Layer for TextLayer {
    fn prepare(&mut self, context: PrepareContext) {
        let PrepareContext {
            assets,
            config,
            device,
            depth,
            ..
        } = context;

        let mut fonts_added = false;

        'sections: for s in &self.sections {
            if !s.disabled {
                for t in &s.text {
                    let font_handle = t.font;

                    if !self.font_lookup.contains_key(&font_handle) {
                        fonts_added = true;
                        break 'sections;
                    }
                }
            }
        }

        if fonts_added {
            self.font_lookup.clear();

            let mut builder = wgpu_glyph::GlyphBrushBuilder::using_fonts(Vec::new())
                .depth_stencil_state(DepthStencilState {
                    format: Texture::DEPTH_FORMAT,
                    depth_write_enabled: true,
                    depth_compare: wgpu::CompareFunction::Less,
                    stencil: wgpu::StencilState::default(),
                    bias: wgpu::DepthBiasState::default(),
                });
            for s in &self.sections {
                if !s.disabled {
                    for t in &s.text {
                        let font_handle = t.font;

                        if self.font_lookup.contains_key(&font_handle) {
                            continue;
                        }

                        match assets.get(&font_handle) {
                            Some(lock) => match *lock {
                                LoadState::Loaded(ref f) => {
                                    let font_id = builder.add_font(f.0.clone());

                                    self.font_lookup.insert(font_handle, font_id);
                                }
                                _ => {}
                            },
                            _ => {}
                        }
                    }
                }
            }

            self.glyph_brush = Some(builder.build(device, config.format))
        }

        if let Some(glyph_brush) = &mut self.glyph_brush {
            for s in &self.sections {
                if !s.disabled {
                    let mut texts = Vec::with_capacity(s.text.len());
                    for t in &s.text {
                        if let Some(font_id) = self.font_lookup.get(&t.font) {
                            texts.push(wgpu_glyph::OwnedText {
                                text: t.text.clone(),
                                font_id: font_id.clone(),
                                scale: wgpu_glyph::ab_glyph::PxScale::from(t.scale),
                                extra: wgpu_glyph::Extra {
                                    color: get_color_rgba(&t.color),
                                    z: depth,
                                },
                            })
                        }
                    }

                    glyph_brush.queue(&wgpu_glyph::OwnedSection {
                        bounds: (s.bounds.width, s.bounds.height),
                        layout: s.layout,
                        screen_position: (s.position.x, s.position.y),
                        text: texts,
                    });
                }
            }
        }
    }

    fn render_translucents(&mut self, context: RenderContext) -> Result<(), wgpu::SurfaceError> {
        let RenderContext {
            size,
            device,
            view,
            depth_texture,
            encoder,
            ..
        } = context;

        let Self {
            glyph_brush,
            staging_belt,
            ..
        } = self;

        if let Some(glyph_brush) = glyph_brush.as_mut() {
            glyph_brush
                .draw_queued(
                    device,
                    staging_belt,
                    encoder,
                    &view,
                    RenderPassDepthStencilAttachment {
                        view: &depth_texture.view,
                        depth_ops: Some(wgpu::Operations {
                            load: wgpu::LoadOp::Load,
                            store: true,
                        }),
                        stencil_ops: None,
                    },
                    size.width as u32,
                    size.height as u32,
                )
                .unwrap();

            staging_belt.finish();
        }

        Ok(())
    }
}

impl TextLayer {
    pub fn add_section(&mut self, section: TextSection) -> Handle<TextSection> {
        let i = self.sections.len();
        self.sections.push(section);
        Handle::new(i)
    }
}
