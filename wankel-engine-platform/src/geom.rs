#[derive(Debug, Clone, Copy)]
pub struct Rect<T> {
    pub x: T,
    pub y: T,
    pub width: T,
    pub height: T,
}

pub trait OfType<T> {
    fn of_type(self) -> T;
}

impl<T, TNew> OfType<Rect<TNew>> for Rect<T>
where
    T: Into<TNew>,
{
    fn of_type(self) -> Rect<TNew> {
        Rect {
            x: self.x.into(),
            y: self.y.into(),
            width: self.width.into(),
            height: self.height.into(),
        }
    }
}

impl<T, TNew> OfType<Offset<TNew>> for Offset<T>
where
    T: Into<TNew>,
{
    fn of_type(self) -> Offset<TNew> {
        Offset {
            x: self.x.into(),
            y: self.y.into(),
        }
    }
}

impl<T, TNew> OfType<Point<TNew>> for Point<T>
where
    T: Into<TNew>,
{
    fn of_type(self) -> Point<TNew> {
        Point {
            x: self.x.into(),
            y: self.y.into(),
        }
    }
}

impl<T, TNew> OfType<Size<TNew>> for Size<T>
where
    T: Into<TNew>,
{
    fn of_type(self) -> Size<TNew> {
        Size {
            width: self.width.into(),
            height: self.height.into(),
        }
    }
}

pub trait TryOfType<T> {
    type Error;
    fn try_of_type(self) -> Result<T, Self::Error>;
}

impl<T, TNew> TryOfType<Rect<TNew>> for Rect<T>
where
    T: TryInto<TNew>,
{
    type Error = T::Error;
    fn try_of_type(self) -> Result<Rect<TNew>, Self::Error> {
        Ok(Rect {
            x: self.x.try_into()?,
            y: self.y.try_into()?,
            width: self.width.try_into()?,
            height: self.height.try_into()?,
        })
    }
}

impl<T, TNew> TryOfType<Offset<TNew>> for Offset<T>
where
    T: TryInto<TNew>,
{
    type Error = T::Error;
    fn try_of_type(self) -> Result<Offset<TNew>, Self::Error> {
        Ok(Offset {
            x: self.x.try_into()?,
            y: self.y.try_into()?,
        })
    }
}

impl<T, TNew> TryOfType<Point<TNew>> for Point<T>
where
    T: TryInto<TNew>,
{
    type Error = T::Error;
    fn try_of_type(self) -> Result<Point<TNew>, Self::Error> {
        Ok(Point {
            x: self.x.try_into()?,
            y: self.y.try_into()?,
        })
    }
}

impl<T, TNew> TryOfType<Size<TNew>> for Size<T>
where
    T: TryInto<TNew>,
{
    type Error = T::Error;
    fn try_of_type(self) -> Result<Size<TNew>, Self::Error> {
        Ok(Size {
            width: self.width.try_into()?,
            height: self.height.try_into()?,
        })
    }
}

impl<T> Rect<T> {
    pub fn into_parts(self) -> (Point<T>, Size<T>) {
        let Rect {
            x,
            y,
            width,
            height,
        } = self;
        (Point { x, y }, Size { width, height })
    }
}

impl<T> Rect<T> {
    pub fn new(x: T, y: T, width: T, height: T) -> Self {
        Self {
            x,
            y,
            width,
            height,
        }
    }
}

impl Rect<u32> {
    pub fn as_f32(self) -> Rect<f32> {
        Rect {
            x: self.x as f32,
            y: self.y as f32,
            width: self.width as f32,
            height: self.height as f32,
        }
    }
}

impl<T> Rect<T>
where
    T: PartialOrd + std::ops::Add<T, Output = T> + Clone,
{
    pub fn contains(&self, point: &Point<T>) -> bool {
        self.x <= point.x
            && self.y <= point.y
            && self.width.clone() + self.x.clone() >= point.x
            && self.height.clone() + self.y.clone() >= point.y
    }
}

impl<T> Rect<T>
where
    T: std::ops::Add<T, Output = T> + std::ops::Sub<T, Output = T> + Clone,
{
    pub fn expanded(&self, amount: T) -> Self {
        Self {
            x: self.x.clone() - amount.clone(),
            y: self.y.clone() - amount.clone(),
            width: self.width.clone() + amount.clone() + amount.clone(),
            height: self.height.clone() + amount.clone() + amount.clone(),
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Point<T> {
    pub x: T,
    pub y: T,
}

impl<T> Point<T> {
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }

    pub fn into_raw(self) -> [T; 2] {
        let Point { x, y } = self;
        [x, y]
    }

    pub fn from_raw([x, y]: [T; 2]) -> Self {
        Self { x, y }
    }

    pub fn min(self) -> T
    where
        T: PartialOrd<T>,
    {
        if self.x < self.y {
            self.x
        } else {
            self.y
        }
    }

    pub fn max(self) -> T
    where
        T: PartialOrd<T>,
    {
        if self.x > self.y {
            self.x
        } else {
            self.y
        }
    }
}

impl<T> Into<(T, T)> for Point<T> {
    fn into(self) -> (T, T) {
        (self.x, self.y)
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Offset<T> {
    pub x: T,
    pub y: T,
}

impl<T> Offset<T> {
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }

    pub fn min(self) -> T
    where
        T: PartialOrd<T>,
    {
        if self.x < self.y {
            self.x
        } else {
            self.y
        }
    }

    pub fn max(self) -> T
    where
        T: PartialOrd<T>,
    {
        if self.x > self.y {
            self.x
        } else {
            self.y
        }
    }
}

impl<T> std::ops::AddAssign<Offset<T>> for Point<T>
where
    T: std::ops::Add<T, Output = T> + Copy,
{
    fn add_assign(&mut self, rhs: Offset<T>) {
        *self = *self + rhs;
    }
}

impl<T> std::ops::Add<Offset<T>> for Point<T>
where
    T: std::ops::Add<T, Output = T>,
{
    type Output = Point<T>;

    fn add(self, rhs: Offset<T>) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl<T> std::ops::Add<Offset<T>> for Rect<T>
where
    T: std::ops::Add<T, Output = T>,
{
    type Output = Rect<T>;

    fn add(self, rhs: Offset<T>) -> Self::Output {
        Rect {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            height: self.height,
            width: self.width,
        }
    }
}

impl<T> std::ops::Add<Point<T>> for Offset<T>
where
    T: std::ops::Add<T, Output = T>,
{
    type Output = Point<T>;

    fn add(self, rhs: Point<T>) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl<T> std::ops::Add<Rect<T>> for Offset<T>
where
    T: std::ops::Add<T, Output = T>,
{
    type Output = Rect<T>;

    fn add(self, rhs: Rect<T>) -> Self::Output {
        Rect {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            height: rhs.height,
            width: rhs.width,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Size<T> {
    pub width: T,
    pub height: T,
}

impl<T> Into<(T, T)> for Size<T> {
    fn into(self) -> (T, T) {
        (self.width, self.height)
    }
}

impl<T> Size<T> {
    pub fn new(width: T, height: T) -> Self {
        Self { width, height }
    }

    pub fn into_raw(self) -> [T; 2] {
        let Self { width, height } = self;
        [width, height]
    }

    pub fn from_raw([width, height]: [T; 2]) -> Self {
        Self { width, height }
    }

    pub fn min(self) -> T
    where
        T: PartialOrd<T>,
    {
        if self.width < self.height {
            self.width
        } else {
            self.height
        }
    }

    pub fn max(self) -> T
    where
        T: PartialOrd<T>,
    {
        if self.width > self.height {
            self.width
        } else {
            self.height
        }
    }
}

impl<T> std::ops::Add<Size<T>> for Point<T> {
    type Output = Rect<T>;

    fn add(self, rhs: Size<T>) -> Self::Output {
        Rect {
            x: self.x,
            y: self.y,
            width: rhs.width,
            height: rhs.height,
        }
    }
}

impl<T> std::ops::Add<Point<T>> for Size<T> {
    type Output = Rect<T>;

    fn add(self, rhs: Point<T>) -> Self::Output {
        Rect {
            x: rhs.x,
            y: rhs.y,
            width: self.width,
            height: self.height,
        }
    }
}
