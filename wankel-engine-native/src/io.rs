use std::io::Read;

use wankel_engine_platform::io::IOContext;

use async_trait::async_trait;

pub struct IO;

#[async_trait(?Send)]
impl IOContext for IO {
    async fn open_file(&self, path: &str) -> Option<Vec<u8>> {
        let mut file = std::fs::File::open(path).ok()?;
        let mut ret = Vec::<u8>::new();

        file.read_to_end(&mut ret).ok()?;

        Some(ret)
    }
}
