use std::sync::{Arc, RwLock};

use wankel_engine_platform::{
    net::{Message, WebsocketResult},
    tracing::{debug, error},
    Error,
};
use wasm_bindgen::{prelude::Closure, JsCast, JsValue};
use web_sys::{ErrorEvent, MessageEvent};

pub enum SocketState {
    Starting(Vec<Message>),
    Running,
    Stopped,
}

pub struct Websocket {
    inner: web_sys::WebSocket,
    state: Arc<RwLock<SocketState>>,
    messages: std::sync::mpsc::Receiver<WebsocketResult>,
    _on_close: Closure<dyn FnMut(JsValue)>,
    _on_error: Closure<dyn FnMut(ErrorEvent)>,
    _on_message: Closure<dyn FnMut(MessageEvent)>,
    _on_open: Closure<dyn FnMut(JsValue)>,
}

impl Websocket {
    pub fn new(url: &str) -> Result<Self, Error> {
        let inner = web_sys::WebSocket::new(url).map_err(|j| Error::new(&format!("{:?}", j)))?;
        inner.set_binary_type(web_sys::BinaryType::Arraybuffer);
        let state = Arc::new(RwLock::new(SocketState::Starting(Vec::new())));
        let (sender, receiver) = std::sync::mpsc::channel();

        let _on_message = Closure::wrap(Box::new({
            let sender = sender.clone();
            move |e: MessageEvent| {
                debug!("Received Message");
                if let Ok(abuf) = e.data().dyn_into::<js_sys::ArrayBuffer>() {
                    debug!("Received Binary Message");
                    let array = js_sys::Uint8Array::new(&abuf);
                    let vec = array.to_vec();
                    sender
                        .send(WebsocketResult::Message(Message::Binary(vec)))
                        .unwrap();
                } else if let Ok(_) = e.data().dyn_into::<web_sys::Blob>() {
                    error!("Received a blob message");
                } else if let Ok(txt) = e.data().dyn_into::<js_sys::JsString>() {
                    debug!("Received Text Message");
                    let s: String = txt.into();
                    sender
                        .send(WebsocketResult::Message(Message::String(s)))
                        .unwrap();
                } else {
                    error!("Received a unknown message");
                }
            }
        }) as Box<dyn FnMut(MessageEvent)>);
        inner.set_onmessage(Some(_on_message.as_ref().unchecked_ref()));

        let _on_error = Closure::wrap(Box::new({
            let sender = sender.clone();
            move |e: ErrorEvent| {
                debug!("Error");
                sender
                    .send(WebsocketResult::Error(Error::new(&e.message())))
                    .unwrap();
            }
        }) as Box<dyn FnMut(ErrorEvent)>);
        inner.set_onerror(Some(_on_error.as_ref().unchecked_ref()));

        let _on_close = Closure::wrap(Box::new({
            let sender = sender.clone();
            let state = state.clone();
            move |_| {
                debug!("Closed");
                let mut lock = state.write().unwrap();
                *lock = SocketState::Stopped;
                sender.send(WebsocketResult::Disconnect).unwrap();
            }
        }) as Box<dyn FnMut(JsValue)>);
        inner.set_onclose(Some(_on_close.as_ref().unchecked_ref()));

        let _on_open = Closure::wrap(Box::new({
            let state = state.clone();
            let inner = inner.clone();
            move |_| {
                debug!("Open");
                let mut lock = state.write().unwrap();

                debug!("Lock");
                if let SocketState::Starting(buffer) = &(*lock) {
                    for m in buffer {
                        match m {
                            Message::String(s) => inner.send_with_str(&s),
                            Message::Binary(b) => inner.send_with_u8_array(&b),
                        }
                        .unwrap()
                    }
                }

                *lock = SocketState::Running;

                debug!("Done Open");
            }
        }) as Box<dyn FnMut(JsValue)>);
        inner.set_onopen(Some(_on_open.as_ref().unchecked_ref()));

        Ok(Self {
            inner,
            state,
            messages: receiver,
            _on_close,
            _on_error,
            _on_message,
            _on_open,
        })
    }

    pub fn get_messages(&mut self) -> Vec<WebsocketResult> {
        let mut ret = Vec::new();
        loop {
            match self.messages.try_recv() {
                Ok(msg) => ret.push(msg),
                Err(std::sync::mpsc::TryRecvError::Empty) => break,
                Err(std::sync::mpsc::TryRecvError::Disconnected) => {
                    ret.push(WebsocketResult::Disconnect);
                    break;
                }
            }
        }

        ret
    }

    pub fn send_to(&mut self, message: Message) -> Result<(), Error> {
        let mut lock = self.state.write().unwrap();
        match &mut *lock {
            SocketState::Starting(ref mut b) => {
                b.push(message);
                Ok(())
            }
            SocketState::Running => match message {
                Message::Binary(b) => self.inner.send_with_u8_array(&b),
                Message::String(s) => self.inner.send_with_str(&s),
            }
            .map_err(|e| Error::new(&format!("{:?}", e))),
            SocketState::Stopped => Err(Error::new("Connection Closed!")),
        }
    }
}
