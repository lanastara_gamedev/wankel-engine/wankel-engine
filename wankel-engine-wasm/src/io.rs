use async_trait::async_trait;
use js_sys::Uint8Array;
use wankel_engine_platform::io::IOContext;
use wasm_bindgen::JsCast;
use wasm_bindgen_futures::JsFuture;
use web_sys::Response;

pub struct IO;

#[async_trait(?Send)]
impl IOContext for IO {
    async fn open_file(&self, path: &str) -> Option<Vec<u8>> {
        let window = web_sys::window().unwrap();
        let resp_value = JsFuture::from(window.fetch_with_str(path)).await.ok()?;

        let resp: Response = resp_value.dyn_into().unwrap();

        let data = JsFuture::from(resp.array_buffer().unwrap()).await.unwrap();
        let bytes = Uint8Array::new(&data).to_vec();

        Some(bytes)
    }
}
