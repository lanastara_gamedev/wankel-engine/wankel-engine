use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, DeriveInput};

#[proc_macro_attribute]
pub fn main(_attr: TokenStream, item: TokenStream) -> TokenStream {
    let input = parse_macro_input!(item as syn::ItemFn);

    let block = input.block;

    let return_type = input.sig.output;

    quote! {
    #[cfg(not(target_arch = "wasm32"))]
    pub fn main() #return_type{
        wankel_engine::init();

        wankel_engine::tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .build()
            .unwrap()
            .block_on(async {
                _main().await

            })
    }

    #[cfg(target_arch = "wasm32")]
    pub fn main()  {}

    #[cfg(target_arch = "wasm32")]
    mod wasm {
        use wankel_engine::wasm_bindgen;
        use wankel_engine::wasm_bindgen::prelude::*;
        use wankel_engine::wasm_bindgen_futures;

        #[wankel_engine::wasm_bindgen::prelude::wasm_bindgen(start)]
        pub async fn wasm_start(){
            wankel_engine::init();

            wankel_engine::tracing::warn!("main returned: {:?}", crate::_main().await);
        }

    }

    async fn _main() #return_type
    #block


            }
    .into()
}

#[proc_macro_derive(Buffer, attributes(buffer))]
pub fn derive_buffer(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    let mut is_instance = false;

    for att in input.attrs {
        if att.path.is_ident("buffer") {
            let meta = att.parse_meta().expect("invalid meta");
            if let syn::Meta::List(l) = meta {
                for i in l.nested {
                    if let syn::NestedMeta::Meta(m) = i {
                        if m.path().is_ident("instance") {
                            is_instance = true;
                        }
                    }
                }
            }
        }
    }

    let ident = input.ident;

    let step_mode = if is_instance {
        quote! {wgpu::VertexStepMode::Instance}
    } else {
        quote! {wgpu::VertexStepMode::Vertex}
    };

    let mut field_stack = vec![];

    let mut attributes = vec![];

    let mut location = 0u32;

    match input.data {
        syn::Data::Struct(data) => {
            for f in data.fields {
                for att in f.attrs {
                    if att.path.is_ident("buffer") {
                        let meta = att.parse_meta().expect("invalid meta");
                        if let syn::Meta::List(meta_list) = meta {
                            for m in meta_list.nested {
                                if let syn::NestedMeta::Meta(meta) = m {
                                    if let syn::Meta::NameValue(name_value) = meta {
                                        if name_value.path.is_ident("location") {
                                            if let syn::Lit::Int(i) = name_value.lit
                                            {
                                                location =
                                                        i.base10_parse().expect(&format!("Invalid Location value {:?}", i));
                                            }else {
                                                panic!("Invalid Location value {:?}", name_value.lit);
                                            }
                                        }else {
                                            panic!("Invalie Attribute value: {:?}", name_value.path);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                let ty = f.ty;

                attributes.push(quote!{
                    wgpu::VertexAttribute{
                        format: <#ty>::VERTEX_FORMAT,
                        offset: std::mem::size_of::<(#(#field_stack ,)*)>() as wgpu::BufferAddress,
                        shader_location: #location
                    }
                });

                location += 1;
                field_stack.push(ty);
            }
        }
        syn::Data::Enum(_) => panic!("Enums are not Supported"),
        syn::Data::Union(_) => panic!("Enums are not Supported"),
    }

    quote! {
    impl Buffer for #ident{
        fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
            wgpu::VertexBufferLayout {
                array_stride: std::mem::size_of::<Self>() as wgpu::BufferAddress,
                step_mode: #step_mode,
                attributes: &[
                    #(#attributes ,)*
                ],
            }
        }
    }
            }
    .into()
}
